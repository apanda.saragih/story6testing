from django import forms
from .models import PostModel
from django.forms import ModelForm, Textarea

class PostForm(forms.ModelForm):
    status = forms.CharField(max_length=300, widget=forms.Textarea)
    class Meta: #untuk aksesing secara langsung class Model lu
        model = PostModel
        fields = [
            'status',
        ]